﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;

namespace Pharma.Mobile.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {

        private bool isBusy;
        public bool IsBusy
        {
            get
            {
                return this.isBusy;
            }

            set
            {
                if (this.isBusy == value)
                {
                    return;
                }

                this.isBusy = value;
                this.NotifyPropertyChanged();
            }
        }



        public ViewModelBase()
        {

        }


        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
